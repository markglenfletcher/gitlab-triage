module Gitlab
  module Triage
    Options = Struct.new(
      :dry_run,
      :policies_file,
      :project_id,
      :token,
      :debug,
      :host_url,
      :api_version
    ) do
      def initialize(*args)
        super

        # Defaults
        self.host_url ||= 'https://gitlab.com'
        self.api_version ||= 'v4'
      end
    end
  end
end
