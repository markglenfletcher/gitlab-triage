# frozen_string_literal: true

require_relative '../command_builders/text_content_builder'

module Gitlab
  module Triage
    module EntityBuilders
      class IssueBuilder
        attr_writer :description, :items

        def initialize(type:, action:, resources:, network:)
          @type = type
          @item_template = action[:item]
          @title_template = action[:title]
          @summary_template = action[:summary]
          @redact_confidentials =
            action[:redact_confidential_resources] != false
          @resources = resources
          @network = network
        end

        def title
          @title ||= build_text(title_resource, @title_template)
        end

        def description
          @description ||= build_text(description_resource, @summary_template)
        end

        def valid?
          title =~ /\S+/
        end

        private

        def title_resource
          { type: @type }
        end

        def description_resource
          title_resource.merge(title: title, items: items)
        end

        def items
          @items ||= @resources.map(&method(:build_item)).join("\n")
        end

        def build_item(resource)
          build_text(resource, @item_template)
        end

        def build_text(resource, template)
          return '' unless template

          CommandBuilders::TextContentBuilder.new(
            template,
            resource: resource,
            network: @network,
            redact_confidentials: @redact_confidentials)
            .build_command.chomp
        end
      end
    end
  end
end
