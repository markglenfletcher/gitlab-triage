require 'active_support/all'
require 'net/protocol'

require_relative 'retryable'
require_relative 'ui'

module Gitlab
  module Triage
    class Network
      include Retryable

      TokenNotFound = Class.new(StandardError)

      attr_reader :options, :adapter

      def initialize(adapter)
        @adapter = adapter
        @options = adapter.options
        @cache = {}
      end

      def query_api_cached(url)
        @cache[url] || @cache[url] = query_api(url)
      end

      def query_api(url)
        response = {}
        resources = []

        begin
          print '.'

          response = execute_with_retry(Net::ReadTimeout) do
            puts Gitlab::Triage::UI.debug "query_api: #{url}" if options.debug

            @adapter.get(token, response.fetch(:next_page_url) { url })
          end

          results = response.delete(:results)

          case results
          when Array
            resources.concat(results)
          else
            resources << results
          end
        end while response.delete(:more_pages)

        resources.map!(&:with_indifferent_access)
      rescue Net::ReadTimeout
        []
      end

      def post_api(url, body)
        execute_with_retry(Net::ReadTimeout) do
          puts Gitlab::Triage::UI.debug "post_api: #{url}" if options.debug

          @adapter.post(token, url, body)
        end

      rescue Net::ReadTimeout
        false
      end

      private

      def token
        options.token || raise(TokenNotFound)
      end
    end
  end
end
