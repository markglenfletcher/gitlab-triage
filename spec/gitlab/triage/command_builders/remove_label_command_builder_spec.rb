require 'spec_helper'

require 'gitlab/triage/command_builders/remove_label_command_builder'

describe Gitlab::Triage::CommandBuilders::RemoveLabelCommandBuilder do
  let(:labels) do
    ['bug', 'feature proposal']
  end

  context '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(labels)
      expect(builder.build_command).to eq('/unlabel ~"bug" ~"feature proposal"')
    end

    it 'outputs an empty string if no labels present' do
      builder = described_class.new([])
      expect(builder.build_command).to eq("")
    end
  end
end
