require 'spec_helper'

require 'gitlab/triage/action'
require 'gitlab/triage/policies/rule_policy'

require 'active_support/core_ext/hash/except'
require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action do
  include_context 'network'

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test rule name', actions: actions_hash },
      [
        { title: 'Issue #0', web_url: 'http://example.com/0' },
        { title: 'Issue #1', web_url: 'http://example.com/1' }
      ],
      network
    )
  end
  let(:action_args) do
    {
      policy: policy,
      network: network
    }
  end
  let(:options) do
    {
      policy: policy,
      network: network,
      dry: dry
    }
  end
  let(:actions_hash) { rules }

  shared_examples 'acting with the right action' do |klass|
    context 'when doing actual run' do
      let(:dry) { false }

      it "uses #{klass} to process it" do
        expect_next_instance_of(klass, action_args) do |summarize|
          expect(summarize).to receive(:act)
        end

        subject.process(**options)
      end
    end

    context 'when doing dry-run' do
      let(:dry) { true }

      it "uses #{klass::Dry} to process it" do
        expect_next_instance_of(klass::Dry, action_args) do |summarize|
          expect(summarize).to receive(:act)
        end

        subject.process(**options)
      end
    end
  end

  describe '.process' do
    context 'when rules contain :summarize' do
      let(:actions_hash) { { summarize: { title: 'title' } } }

      it_behaves_like 'acting with the right action',
        described_class::Summarize
    end

    context 'when rules contain :mention' do
      let(:actions_hash) { { mention: ['user'] } }

      it_behaves_like 'acting with the right action',
        described_class::Comment
    end

    context 'when rules contain :summarize and :mention' do
      let(:actions_hash) do
        {
          mention: ['user'],
          summarize: { title: 'title' }
        }
      end

      context 'when doing actual run' do
        let(:dry) { false }

        it "uses Summarize and Comment to process it" do
          expect_next_instance_of(
            described_class::Summarize, action_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          expect_next_instance_of(
            described_class::Comment, action_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          subject.process(**options)
        end
      end

      context 'when doing dry-run' do
        let(:dry) { true }

        it "uses Summarize::Dry and Comment::Dry to process it" do
          expect_next_instance_of(
            described_class::Summarize::Dry, action_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          expect_next_instance_of(
            described_class::Comment::Dry, action_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          subject.process(**options)
        end
      end
    end
  end
end
