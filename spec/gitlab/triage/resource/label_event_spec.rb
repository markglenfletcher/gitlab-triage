require 'spec_helper'

require 'gitlab/triage/resource/label_event'

describe Gitlab::Triage::Resource::LabelEvent do
  include_context 'network'

  let(:resource) do
    {
      id: 1,
      resource_type: 'Issue',
      resource_id: 135,
      action: 'add',
      created_at: created_at.iso8601,
      label: label_resource
    }
  end

  let(:label_resource) do
    {
      id: 3,
      project_id: 123,
      name: 'bug',
      description: 'a bug',
      color: '#d9534f',
      priority: 10,
      added_at: created_at.iso8601
    }
  end

  let(:created_at) { Time.new(2017, 1, 1) }

  subject { described_class.new(resource, network: network) }

  it_behaves_like 'resource fields'

  describe '#label' do
    it 'returns a Label resource object with the label attributes' do
      label = subject.label

      expect(label).to be_a(Gitlab::Triage::Resource::Label)

      %i[id project_id name description color priority].each do |field|
        expect(label.public_send(field)).to eq(label_resource[field])
      end

      expect(label.added_at).to eq(subject.created_at)
    end
  end
end
