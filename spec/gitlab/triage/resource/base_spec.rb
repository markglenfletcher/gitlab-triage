# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/base'

describe Gitlab::Triage::Resource::Base do
  include_context 'network'

  let(:model) { Class.new(described_class) }
  let(:instance) { model.new(resource, **options) }
  let(:options) { { network: network } }
  let(:name) { 'Gitlab::Triage::Resource::TestModel' }
  let(:resource) { {} }

  before do
    allow(model).to receive(:name).and_return(name)
  end

  subject { instance }

  describe '#url' do
    context 'when resource has project_id' do
      let(:resource) { { project_id: 1 } }

      it 'generates the url based on class name and project source' do
        url = subject.__send__(:url)

        expect(url).to eq("#{base_url}/projects/1/test_models?per_page=100")
      end

      it 'generates the url with extra query params' do
        url = subject.__send__(:url, state: 'active')

        expect(url).to eq(
          "#{base_url}/projects/1/test_models?per_page=100&state=active")
      end
    end

    context 'when resource has group_id' do
      let(:resource) { { group_id: 2 } }

      it 'generates the url based on class name and group source' do
        url = subject.__send__(:url)

        expect(url).to eq(
          "#{base_url}/groups/2/test_models?per_page=100")
      end
    end
  end

  describe '#resource_url' do
    context 'when resource has project_id' do
      let(:resource) { { project_id: 1, iid: 5 } }

      it 'generates the resource url based on class name, source, and iid' do
        url = subject.__send__(:resource_url, sub_resource_type: 'notes')

        expect(url).to eq(
          "#{base_url}/projects/1/test_models/5/notes?per_page=100")
      end

      it 'generates the resource url with extra query params' do
        url = subject.__send__(
          :resource_url,
          sub_resource_type: 'notes',
          params: { state: 'active' })

        expect(url).to eq(
          "#{base_url}/projects/1/test_models/5/notes?per_page=100&state=active")
      end
    end

    context 'when resource has group_id' do
      let(:resource) { { group_id: 2, iid: 7 } }

      it 'generates the url based on class name and group source' do
        url = subject.__send__(:resource_url, sub_resource_type: 'notes')

        expect(url).to eq(
          "#{base_url}/groups/2/test_models/7/notes?per_page=100")
      end
    end
  end

  describe '#redact_confidential_attributes?' do
    subject { instance.__send__(:redact_confidential_attributes?) }

    context 'when the resource is confidential' do
      let(:resource) { { confidential: true } }

      it 'returns true' do
        expect(subject).to eq(true)
      end

      context 'when redact_confidentials is disabled' do
        let(:options) { { redact_confidentials: false } }

        it 'returns false' do
          expect(subject).to eq(false)
        end
      end

      context 'when there is a parent with confidential resource and redact_confidentials is disabled' do
        let(:parent) { model.new(confidential: true) }
        let(:options) { { parent: parent, redact_confidentials: false } }

        it 'returns true because confidentiality is inherited' do
          expect(subject).to eq(true)
        end
      end
    end

    context 'when the resource is public' do
      let(:resource) { { confidential: false } }

      it 'returns false' do
        expect(subject).to eq(false)
      end

      context 'when redact_confidentials is enabled' do
        let(:options) { { redact_confidentials: true } }

        it 'returns false' do
          expect(subject).to eq(false)
        end
      end

      context 'when there is a parent with confidential resource and redact_confidentials is disabled' do
        let(:parent) { model.new(confidential: true) }
        let(:options) { { parent: parent, redact_confidentials: false } }

        it 'returns true because confidentiality is inherited' do
          expect(subject).to eq(true)
        end
      end
    end
  end

  describe '#network' do
    subject { instance.__send__(:network) }

    context 'when it has network' do
      let(:options) { { network: network } }

      it 'returns the network object' do
        expect(subject).to eq(network)
      end
    end

    context 'when the parent has network' do
      let(:parent) { model.new({}, network: network) }
      let(:options) { { parent: parent } }

      it 'returns the network object from parent' do
        expect(subject).to eq(network)
        expect(subject).to eq(parent.__send__(:network))
      end
    end
  end
end
