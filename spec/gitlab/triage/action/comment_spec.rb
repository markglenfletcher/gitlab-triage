require 'spec_helper'

require 'gitlab/triage/action/comment'
require 'gitlab/triage/policies/rule_policy'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::Comment do
  include_context 'network'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ]
  end
  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test comment rule', actions: actions_hash },
      resources,
      network
    )
  end
  let(:actions_hash) do
    {
      mention: 'user',
      labels: ['label']
    }
  end
  let(:body) do
    <<~TEXT.chomp
      /label ~"label"

      /cc @user
    TEXT
  end

  subject do
    described_class.new(
      policy: policy, network: network)
  end

  describe '#act' do
    it 'posts the right issue' do
      stub_post = stub_api(
        :post,
        "http://test.com/api/v4/projects/123/issues/notes",
        body: { body: body }
      )

      subject.act

      assert_requested(stub_post, times: resources.size)
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end

    context 'when comment only has whitespaces' do
      let(:actions_hash) do
        {
          comment: "\n \n \n"
        }
      end

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end

    context 'when adding a comment with leading and trailing whitespaces' do
      let(:actions_hash) do
        {
          comment: <<~TEXT

            This is the first line.
            This is the last line.

          TEXT
        }
      end

      let(:body) do
        <<~TEXT.chomp
          This is the first line.
          This is the last line.
        TEXT
      end

      it 'posts the comment without leading and trailing whitespaces' do
        stub_post = stub_api(
          :post,
          "http://test.com/api/v4/projects/123/issues/notes",
          body: { body: body }
        )

        subject.act

        assert_requested(stub_post, times: resources.size)
      end
    end
  end
end
