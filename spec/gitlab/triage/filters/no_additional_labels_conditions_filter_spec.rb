require 'spec_helper'

require 'gitlab/triage/filters/no_additional_labels_conditions_filter'

describe Gitlab::Triage::Filters::NoAdditionalLabelsConditionsFilter do
  let(:labels) { %w[label1 Label\ 2 !label] }
  let(:condition) { [] }
  let(:resource) do
    {
      labels: labels
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'

  context 'validation' do
    it 'throws an exception when conditions is not an Array' do
      expect do
        described_class.new(resource, 'not_an_array')
      end.to raise_error(ArgumentError)
    end
  end

  context '#resource_value' do
    it 'checks resource labels' do
      expect(subject.resource_value).to eq(labels)
    end
  end

  context '#calculate' do
    context 'when condition elements are not in #resource_value' do
      let(:condition) { %w[another\ label] }

      it 'has to be false' do
        expect(subject.calculate).to be_falsey
      end
    end

    context 'when the condition is equal to #resource_value' do
      let(:condition) { labels }

      it 'has to be true' do
        expect(subject.calculate).to be_truthy
      end
    end

    context 'when the condition match only one element of #resource_value' do
      let(:condition) { %w[!label label_not_present] }

      it 'has to be false' do
        expect(subject.calculate).to be_falsey
      end
    end

    context 'when the condition is a superset of #resource_value' do
      let(:condition) { labels + %w[label_not_present] }

      it 'has to be true' do
        expect(subject.calculate).to be_truthy
      end
    end

    context 'when the condition is a subset of #resource_value' do
      let(:condition) { labels - %w[label1] }

      it 'has to be false' do
        expect(subject.calculate).to be_falsey
      end
    end
  end
end
